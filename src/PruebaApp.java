import java.util.ArrayList;
import java.util.List;

public class PruebaApp {

    private List<String> lista;

    public void llenarLista() {
        lista = new ArrayList<>();
        lista.add("Jesus");
        lista.add("Javier");
        lista.add("Angel");
        lista.add("Fernanda");
        lista.add("Andrea");
        lista.add("Vicky");
        lista.add("Pamela");
        lista.add("Yissela");
        lista.add("Ariel");
        lista.add("Jorge");
        lista.add("Sebastian");
    }

    public void usarForEach() {
        lista.forEach(System.out::println);
    }

    public void usarRemoveIf() {
        lista.removeIf(x -> x.equalsIgnoreCase("Jesus"));
    }

    public void usarSort() {
        lista.sort((x, y) -> x.compareToIgnoreCase(y));
    }

    public static void main(String[] args) {

        PruebaApp app = new PruebaApp();
        System.out.println("--------------");
        app.llenarLista();
        app.usarForEach();
        System.out.println("--------------");
        app.usarRemoveIf();
        app.usarForEach();
        System.out.println("--------------");
        app.usarSort();
        app.usarForEach();

    }

}